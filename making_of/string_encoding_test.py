import string
import zlib

"""
0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_.
0123456789ABCD FGH JKLMN PQRST V XY _.
12345678901234 567 89012 34567 8 90 12 = 32
0123456789ABCD3FGH1JKLMN0PQRSTVVVXY2_.

1024 bytes = 1587 5-bit chars + 256 byte dictionary

"""


s = "This is my small string encoding system test. It is (C) by me 1992.  " * 23

t = \
'................................'\
' ...............0123456789......'\
'.ABCD3FGH1JKLMN0PQRSTVVVXY2.... '\
'.ABCD3FGH1JKLMN0PQRSTVVVXY2.... '\
'................................'\
'................................'\
'................................'\
'................................'


r = string.translate(s, t)

q = 'Scrolling or Panning refers to any attempt to display a scene that is larger than what fits in a single screen. There are many potential challenges with scrolling, like choosing what the player needs to see, what we as designers would like the player to focus on, and how to do it in a way that\xe2\x80\x99s fluid and comfortable for the player.\n\nWhile I\xe2\x80\x99m going to focus on 2D camera systems, many of these general concepts apply to 3D as well. The fovea-centralis is the receptor inside our eyeball responsible for sharp and detailed central vision. The second and third receptor belts, the parafovea and perifovea, excel in reducing images and motions to patterns allowing changes to be quickly recognized, including ala, allowing an alert response while the Visual Cortex is deciphering the input. Training your brain, especially by tying the shift in peripheral vision to the controls, proves useful over time.\n\n\n\nThe Vestibular system located in our inner ear, is responsible for maintaining balance and providing spatial orientation. The signals it sends allow the body to maintain its bention: Use the camera to provide sufficient game info and feedback (what the player needs to see)\n\nInteraction: Provide clear player control on what\xe2\x80\x99s displayed, make background changes predictable and tightly bound to controls (what the player wants to see)\n\nComfort: Ease and contextualize background changes (how to reconcile those needs smoothly and comfortably) ority over the main character. This dictates that the attention should be directed at and now we need 40 more and now it is 123456'
z = zlib.compress(q, 9)
